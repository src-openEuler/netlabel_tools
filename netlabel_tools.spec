Summary: Configuration tools for the Linux NetLabel subsystem
Name:    netlabel_tools
Version: 0.30.0
Release: 6
License: GPLv2
Source: https://github.com/netlabel/netlabel_tools/releases/download/v%{version}/%{name}-%{version}.tar.gz
URL: https://github.com/netlabel/netlabel_tools

Requires: kernel libnl3
Requires(post): systemd
Requires(preun): systemd
Requires(postun): systemd
BuildRequires:  gcc kernel-headers libnl3-devel doxygen systemd

%description
NetLabel is a kernel subsystem which implements explicit packet labeling
protocols such as CIPSO for Linux. Packet labeling is used in secure networks
to mark packets with the security attributes of the data they contain. This
package provides the necessary user space tools to query and configure the
kernel subsystem.

%prep
%autosetup %{name}-%{version}

%build
%configure
%{make_build} VERBOSE=1

%install
rm -rf "%{buildroot}"
%{make_install}

%post
%systemd_post netlabel.service

%preun
%systemd_preun netlabel.service

%postun
%systemd_postun netlabel.service

%files
%license LICENSE
%doc README CHANGELOG SUBMITTING_PATCHES
%attr(0644,root,root) %{_mandir}/man8/*
%attr(0755,root,root) %{_sbindir}/netlabelctl
%attr(0755,root,root) %{_sbindir}/netlabel-config
%attr(0644,root,root) %{_unitdir}/netlabel.service
%attr(0644,root,root) %config(noreplace) /etc/netlabel.rules

%changelog
* Fri Nov 22 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.30.0-6j
- Package init
